<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="GroupSpotter" version="2.0.2" date="02/06/2011" >

		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Crestor" email="crestor@web.de" />
		<Description text="" />

		<Dependencies>
			<Dependency name="Minmap" optional="true" forceEnable="false" />
			<Dependency name="CMap" optional="true" forceEnable="false" />
			<Dependency name="EA_Window_OverheadMap" optional="true" forceEnable="false" />
			<Dependency name="EA_Window_WorldMap" optional="true" forceEnable="false" />
			<Dependency name="MiniWorldMap" optional="true" forceEnable="false" />
			<Dependency name="YakUI" optional="true" forceEnable="false" />
		</Dependencies>

		<Files>
			<File name="textures/Textures.xml" />
			<File name="GroupSpotter.xml" />
			<File name="Settings.xml" />
			<File name="GroupSpotter.lua" />
			<File name="Healcircles.lua" />
			<File name="Settings.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="GroupSpotter.Initialize" />
		</OnInitialize>
		<SavedVariables>
			<SavedVariable name="GroupSpotterSettings" />
		</SavedVariables>
		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="GROUPING" />
				<Category name="MAP" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
