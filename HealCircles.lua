GroupSpotter.HealCircles =
{
	templatename = "GroupSpotter_HealCircleTemplate",
	templatewidth = 250,
	registeredHealCircles = {},
}

GroupSpotter.HealCircles.__index = GroupSpotter.HealCircles

function GroupSpotter.InitializeHealCircles()
	for _, m in ipairs(GroupSpotter.Maps) do
		if ( m.useHealCircle ) then
			local newHC = GroupSpotter.HealCircles:Create( m )
			table.insert( GroupSpotter.HealCircles.registeredHealCircles, newHC )
		end
	end

	local zoomlevel = GetOverheadMapZoomLevel()
	for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
		hc:ChangeZoomLevel( zoomlevel )
		hc:UpdateVisibility()
		hc:UpdateSize()
	end
end

function GroupSpotter.OnZoomLevelChanged()
	local zoomlevel = GetOverheadMapZoomLevel()
	for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
		hc:ChangeZoomLevel( zoomlevel )
		hc:UpdateVisibility()
	end
end

function GroupSpotter.ChangeMode( newMode )
	if ( newMode ~= nil ) then
		GroupSpotterSettings.HealCircles.currentmode = newMode
	else
		local currMode = GroupSpotterSettings.HealCircles.currentmode
		if ( currMode < 0 ) then
			GroupSpotterSettings.HealCircles.currentmode = 0
		elseif ( currMode < 2 ) then
			GroupSpotterSettings.HealCircles.currentmode = currMode + 1
		else
			GroupSpotterSettings.HealCircles.currentmode = 0
		end
	end

	for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
		hc:UpdateVisibility()
	end

	GroupSpotter.Settings.UpdateModeCheckBoxes()
end

function GroupSpotter.UpdateColors()
	for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
		hc:UpdateColor()
	end
end

function GroupSpotter.ChangeLinkedWithGroupSpotter( linked )
	GroupSpotterSettings.HealCircles.linkwithgroupspotter = linked

	for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
		hc:UpdateVisibility()
	end
end

function GroupSpotter.HealCircles:Create( data )
	local NewHealCircle = setmetatable ({}, self)

	NewHealCircle.mapwindowname = data.MapName
	NewHealCircle.windowname = "GS_HealCircle" .. data.MapName
	NewHealCircle.ShouldShow = data.ShouldShow

	NewHealCircle.Anchor = data.Anchor

	if ( not DoesWindowExist(NewHealCircle.windowname) ) then
		CreateWindowFromTemplate( NewHealCircle.windowname, GroupSpotter.HealCircles.templatename, data.MapName )
	end

	WindowClearAnchors( NewHealCircle.windowname )
	WindowAddAnchor(	NewHealCircle.windowname,
						NewHealCircle.Anchor.Point,
						NewHealCircle.Anchor.relativeTo,
						NewHealCircle.Anchor.relativePoint,
						NewHealCircle.Anchor.xOffset,
						NewHealCircle.Anchor.yOffset
					)

	if(data.ZoomFunctions)
	then
		for func,base in pairs(data.ZoomFunctions)
		do
			local original=base[func]
			if(original)
			then
				base[func]=function(...)
					original(...)
					GroupSpotter.OnZoomLevelChanged()
				end
			end
		end
	end
	if (data.ResizeFunctions)
	then
		for func,base in pairs(data.ResizeFunctions)
		do
			local original=base[func]
			if(original)
			then
				base[func]=function(...)
					original(...)
					WindowForceProcessAnchors(NewHealCircle.mapwindowname)
					NewHealCircle:UpdateSize()
				end
			end
		end
	end

	return NewHealCircle
end

function GroupSpotter.HealCircles:ChangeZoomLevel( zoomLevel )
	DynamicImageSetTexture(self.windowname .. "Single", "GroupSpotter_HC" .. zoomLevel .."single", 0, 0)
	DynamicImageSetTexture(self.windowname .. "Group", "GroupSpotter_HC" .. zoomLevel .."grp", 0, 0)
end

function GroupSpotter.HealCircles:UpdateVisibility()
	local Visible=WindowGetShowing(self.windowname)
	local ShouldShow=	GroupSpotterSettings.HealCircles.currentmode > 0 and
						WindowGetShowing(self.mapwindowname) and
						self:ShouldShow() and
						(GroupSpotterSettings.HealCircles.linkwithgroupspotter and GroupSpotterSettings.active or not GroupSpotterSettings.HealCircles.linkwithgroupspotter)
	if(Visible~=ShouldShow)
	then
		WindowSetShowing(self.windowname,ShouldShow)
	end
	if(ShouldShow)
	then
		self:UpdateColor()
	end
end

function GroupSpotter.HealCircles:UpdateSize()
	local mapsizewidth,_ = WindowGetDimensions( self.mapwindowname )
	local circlescale = mapsizewidth / GroupSpotter.HealCircles.templatewidth
	local scale = WindowGetScale( self.mapwindowname ) * circlescale

	WindowSetScale(self.windowname, scale)
	WindowSetScale(self.windowname .. "Single", scale)
	WindowSetScale(self.windowname .. "Group", scale)
end

function GroupSpotter.HealCircles:UpdateColor()
	if ( GroupSpotterSettings.HealCircles.currentmode > 0 ) then
		local modecolor = GroupSpotterSettings.HealCircles.modesettings[GroupSpotterSettings.HealCircles.currentmode]

		WindowSetTintColor( self.windowname .. "Single", modecolor.single.r, modecolor.single.g, modecolor.single.b )
		WindowSetAlpha( self.windowname .. "Single", modecolor.single.alpha )
		WindowSetTintColor( self.windowname .. "Group", modecolor.group.r, modecolor.group.g, modecolor.group.b )
		WindowSetAlpha( self.windowname .. "Group", modecolor.group.alpha )
	end
end
