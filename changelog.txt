
== Changelog ==

=== Version 2.0.2: ===
* fixed moved Objective ID

=== Version 2.0.1: ===
* updated for Minmap 1.5.2

=== Version 2.00: ===
* added Heal Circle Map Overlay for Minmap, Mythic Minimap, CustomMap
* added MiniWorldMap integrated into YakUI

=== Version 1.00.1: ===
* updated for 1.3.1

=== Version 1.00: ===
* initial release
