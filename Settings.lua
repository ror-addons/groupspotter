GroupSpotter.Settings = {}

local Settings

local windowname = "GroupSpotterSettingsWindow"

local function SetLabels()
	LabelSetText(windowname .. "TitleBarText", L"GroupSpotter Settings:" )
	LabelSetText(windowname .. "_Heading1", L"Single Heal Circle:")
	LabelSetText(windowname .. "_Heading2", L"Group Heal Circle:")
	LabelSetText(windowname .. "_ModeCheckBoxText1", L"HealCircles mode:     1")
	LabelSetText(windowname .. "_ModeCheckBoxText2", L"2")
	LabelSetText(windowname .. "_ModeCheckBoxText0", L"off")
	LabelSetText(windowname .. "_SpotterCheckBoxText", L"GroupSpotter active:")
	LabelSetText(windowname .. "_LinkCheckBoxText", L"HealCircles linked:")
end

local function UpdateColorLabels()
	local mode = GroupSpotterSettings.HealCircles.currentmode
	if ( mode > 0 and mode < 3 ) then
		LabelSetText(windowname.."_singleRed", L"Red: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.r)
		LabelSetText(windowname.."_singleGreen", L"Green: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.g)
		LabelSetText(windowname.."_singleBlue", L"Blue: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.b)
		LabelSetText(windowname.."_singleAlpha", L"Alpha: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.alpha)

		LabelSetText(windowname.."_grpRed", L"Red: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.r)
		LabelSetText(windowname.."_grpGreen", L"Green: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.g)
		LabelSetText(windowname.."_grpBlue", L"Blue: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.b)
		LabelSetText(windowname.."_grpAlpha", L"Alpha: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.alpha)
	else
		LabelSetText(windowname.."_singleRed", L"")
		LabelSetText(windowname.."_singleGreen", L"")
		LabelSetText(windowname.."_singleBlue", L"")
		LabelSetText(windowname.."_singleAlpha", L"")

		LabelSetText(windowname.."_grpRed", L"")
		LabelSetText(windowname.."_grpGreen", L"")
		LabelSetText(windowname.."_grpBlue", L"")
		LabelSetText(windowname.."_grpAlpha", L"")
	end
end

local function UpdateSliderPos()

	local disableSlider = ( GroupSpotterSettings.HealCircles.currentmode == 0 )

	if ( GroupSpotterSettings.HealCircles.currentmode == 0 ) then
		SliderBarSetCurrentPosition( windowname.."_SlidergrpR" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpG" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpB" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpAlpha" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleR" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleG" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleB" , 0)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleAlpha" , 0)

		SliderBarSetDisabledFlag( windowname.."_SlidergrpR" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpG" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpB" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpAlpha" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleR" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleG" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleB" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleAlpha" , disableSlider)
	else
		SliderBarSetDisabledFlag( windowname.."_SlidergrpR" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpG" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpB" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidergrpAlpha" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleR" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleG" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleB" , disableSlider)
		SliderBarSetDisabledFlag( windowname.."_SlidersingleAlpha" , disableSlider)

		local clr = GroupSpotterSettings.HealCircles.modesettings[GroupSpotterSettings.HealCircles.currentmode]
		SliderBarSetCurrentPosition( windowname.."_SlidergrpR" , clr.group.r / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpG" , clr.group.g / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpB" , clr.group.b / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidergrpAlpha" , clr.group.alpha)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleR" , clr.single.r / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleG" , clr.single.g / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleB" , clr.single.b / 255)
		SliderBarSetCurrentPosition( windowname.."_SlidersingleAlpha" , clr.single.alpha)
	end

	UpdateColorLabels()
end

local function UpdateSettings()
	local mode = GroupSpotterSettings.HealCircles.currentmode

	ButtonSetPressedFlag( windowname.."_SpotterCheckBoxButton", GroupSpotterSettings.active )
	ButtonSetPressedFlag( windowname.."_LinkCheckBoxButton", GroupSpotterSettings.HealCircles.linkwithgroupspotter )

	GroupSpotter.Settings.UpdateModeCheckBoxes()
end

function GroupSpotter.InitializeSettingsWindow()
	CreateWindow(windowname,false)
	WindowSetShowing(windowname,false)
	WindowSetHandleInput(windowname,true)

	SetLabels()
	DynamicImageSetTexture("GroupSpotterSettingsWindowBtnBG","GroupSpotter_BtnBGOff",0,0)
	if(GroupSpotter.firstrun)then GroupSpotter.Settings.OpenWindow() end
end

function GroupSpotter.Settings.ToggleWindow()
	if ( WindowGetShowing(windowname) ) then
		GroupSpotter.Settings.CloseWindow()
	else
		GroupSpotter.Settings.OpenWindow()
	end
end

function GroupSpotter.Settings.OpenWindow()
	UpdateSettings()
	WindowSetShowing(windowname,true)
end

function GroupSpotter.Settings.CloseWindow()
	WindowSetShowing(windowname,false)
end

function GroupSpotter.Settings.ToggleGroupSpotter()
	GroupSpotter.toggle()
end

function GroupSpotter.Settings.toggleCheckBox()
	local actwnd		= SystemData.ActiveWindow.name
	local buttonName	= actwnd.."Button"

	local pressed = ButtonGetPressedFlag( buttonName )
	ButtonSetPressedFlag( buttonName, not pressed )

	if (actwnd == windowname.."_LinkCheckBox") then
		GroupSpotter.ChangeLinkedWithGroupSpotter(not pressed)
	elseif (actwnd == windowname.."_ModeCheckBox1") then
		GroupSpotter.ChangeMode( 1 )
	elseif (actwnd == windowname.."_ModeCheckBox2") then
		GroupSpotter.ChangeMode( 2 )
	elseif (actwnd == windowname.."_ModeCheckBox0") then
		GroupSpotter.ChangeMode( 0 )
	elseif (actwnd == windowname.."_SpotterCheckBox") then
		GroupSpotter.toggle()
	end
end

function GroupSpotter.Settings.UpdateModeCheckBoxes()
	local mode = GroupSpotterSettings.HealCircles.currentmode

	ButtonSetPressedFlag( windowname.."_ModeCheckBox1Button", (mode == 1) )
	ButtonSetPressedFlag( windowname.."_ModeCheckBox2Button", (mode == 2) )
	ButtonSetPressedFlag( windowname.."_ModeCheckBox0Button", (mode == 0) )

	UpdateSliderPos()
end

function GroupSpotter.Settings.colorslidersingleR(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].single.r = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_singleRed", L"Red: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.r)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.colorslidersingleG(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].single.g = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_singleGreen", L"Green: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.g)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.colorslidersingleB(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].single.b = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_singleBlue", L"Blue: "..GroupSpotterSettings.HealCircles.modesettings[mode].single.b)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.SlidersingleAlpha(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		local alpha  = (math.floor(pos*10+0.5))/10
		alpha = wstring.format( L"%0.1f", towstring(alpha) )
		alpha = tonumber(alpha)

		GroupSpotterSettings.HealCircles.modesettings[mode].single.alpha = alpha
		LabelSetText(windowname.."_singleAlpha", L"Alpha: "..alpha)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.colorslidergrpR(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].group.r = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_grpRed", L"Red: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.r)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.colorslidergrpG(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].group.g = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_grpGreen", L"Green: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.g)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.colorslidergrpB(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		GroupSpotterSettings.HealCircles.modesettings[mode].group.b = math.floor(255 * pos + 0.5)
		LabelSetText(windowname.."_grpBlue", L"Blue: "..GroupSpotterSettings.HealCircles.modesettings[mode].group.b)
		GroupSpotter.UpdateColors()
	end
end

function GroupSpotter.Settings.SlidergrpAlpha(pos)
	local mode = GroupSpotterSettings.HealCircles.currentmode

	if ( mode > 0 and mode < 3 ) then
		local alpha  = (math.floor(pos*10+0.5))/10
		alpha = wstring.format( L"%0.1f", towstring(alpha) )
		alpha = tonumber(alpha)

		GroupSpotterSettings.HealCircles.modesettings[mode].group.alpha = alpha
		LabelSetText(windowname.."_grpAlpha", L"Alpha: "..alpha)
		GroupSpotter.UpdateColors()
	end
end
