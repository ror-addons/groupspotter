GroupSpotter={}

local k,l

local pins=
{
	[SystemData.MapPips.DEFAULT]=true,
	[SystemData.MapPips.LANDMARK]=true,
	[SystemData.MapPips.WAR_CAMP]=true,
	[SystemData.MapPips.PUBLIC_QUEST]=true,
	[SystemData.MapPips.OBJECTIVE]=true,
	[SystemData.MapPips.KEEP]=true,
	[SystemData.MapPips.PLAYER]=true,
	[SystemData.MapPips.GROUP_MEMBER]=true,
}

function GroupSpotter.Initialize()
	CreateWindow("GroupSpotter", true)
	LayoutEditor.RegisterWindow("GroupSpotter", L"GroupSpotter", L"Toggles Mini/Worldmap Displaysettings",false,false,true,nil)
	DynamicImageSetTextureDimensions("GroupSpotterBtn",48,48)
	DynamicImageSetTextureDimensions("GroupSpotterBtnBG",48,48)
	DynamicImageSetTexture("GroupSpotterBtn","GroupSpotter_BtnOff",0,0)
	DynamicImageSetTexture("GroupSpotterBtnBG","GroupSpotter_BtnBGOff",0,0)
	WindowSetGameActionData("GroupSpotter",GameData.PlayerActions.COMMAND_TEXT,0, L"/script GroupSpotter.toggle()")
	WindowSetGameActionTrigger("GroupSpotter",GetActionIdFromName("ACTION_BAR_113"))
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "GroupSpotter.RegisterMaps")
	RegisterEventHandler(SystemData.Events.ENTER_WORLD, "GroupSpotter.RegisterMaps")

	GroupSpotter.VersionHandler()

	GroupSpotter.InitializeSettingsWindow()
end

function GroupSpotter.RegisterMaps()
	GroupSpotter.Maps = {}

	if(EA_Window_OverheadMap~=nil)
	then
		local Map =
		{
			MapName = "EA_Window_OverheadMapMapDisplay",
			MapPins = EA_Window_OverheadMap.Settings.mapPinFilters,
			useHealCircle = true,
			Anchor	=
			{
				Point			= "center",
				relativePoint	= "center",
				xOffset			= 0,
				yOffset			= 0,
				relativeTo		= "EA_Window_OverheadMapMapDisplayFrame",
			},
			ZoomFunctions={SlideZoom=EA_Window_OverheadMap},
			ShouldShow=function() return true end
		}
		table.insert(GroupSpotter.Maps, Map)
	end

	if(EA_Window_WorldMap~=nil)
	then
		local Map =
		{
			MapName = "EA_Window_WorldMapZoneViewMapDisplay",
			MapPins = EA_Window_WorldMap.Settings.mapPinFilters,
			useHealCircle = false,
		}
		table.insert(GroupSpotter.Maps, Map)
	end

	if(Minmap~=nil)
	then
		local Map =
		{
			MapName = "MinmapMapDisplay",
			MapPins = MinmapSettings.mapPinFilters,
			useHealCircle = true,
			Anchor	=
			{
				Point			= "topleft",
				relativePoint	= "topleft",
				xOffset			= 0,
				yOffset			= 0,
				relativeTo		= "Minmap",
			},
			ZoomFunctions={ZoomIn=Minmap,ZoomOut=Minmap},
			ResizeFunctions={UpdateMapScale=Minmap},
			ShouldShow=function() return MinmapSettings.zoomlevel <= 1 end
		}
		table.insert(GroupSpotter.Maps, Map)
	end

	if(CMapWindow~=nil)
	then
		local Map =
		{
			MapName = "CMapWindowMapDisplay",
			MapPins = CMapWindow.VisSettings.Pinfilter,
			useHealCircle = true,
			Anchor	=
			{
				Point			= "center",
				relativePoint	= "center",
				xOffset			= 0,
				yOffset			= 0,
				relativeTo		= "CMapWindowMapDisplay",
			},
			ZoomFunctions={SlideZoom=CMapWindow},
			ResizeFunctions={OnResizeEnd=CMapWindow, UpdateMap=CMapWindow},
			ShouldShow=function() return CMapWindow.VisSettings["showzoombuttons"].zoomlevel <= 1 end
		}
		table.insert(GroupSpotter.Maps, Map)
		local WMap =
		{
			MapName = "CMapWindowWMap",
			MapPins = EA_Window_WorldMap.Settings.mapPinFilters,
			useHealCircle = false,
		}
		table.insert(GroupSpotter.Maps, WMap)
	end

	if(MiniWorldMap~=nil)
	then
		local Map =
		{
			MapName = "MiniWorldMap",
			MapPins = EA_Window_WorldMap.Settings.mapPinFilters,
			useHealCircle = false,
		}
		table.insert(GroupSpotter.Maps, Map)
	end

	if(yakui~=nil and yakui.MWMUpdateMap~=nil)
	then
		local Map =
		{
			MapName = "yakuiMWM",
			MapPins = EA_Window_WorldMap.Settings.mapPinFilters,
			useHealCircle = false,
		}
		table.insert(GroupSpotter.Maps, Map)
	end

	GroupSpotter.toggle(GroupSpotterSettings.active)
	GroupSpotter.InitializeHealCircles()
end

function GroupSpotter.OnMouseOverStart()
	DynamicImageSetTexture("GroupSpotterBtnBG","GroupSpotter_BtnBGOn",0,0)
	DynamicImageSetTexture("GroupSpotterSettingsWindowBtnBG","GroupSpotter_BtnBGOn",0,0)

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )

	local row = 1
	local column = 1

	Tooltips.SetTooltipText( row, column, L"Left click to toggle GroupSpotter" )
	Tooltips.SetTooltipText( row+1, column, L"Right click to switch heal circle display mode" )
	Tooltips.SetTooltipText( row+2, column, L"Middle click to open settings")

	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT)
end

function GroupSpotter.OnMouseOverEnd()
	DynamicImageSetTexture("GroupSpotterBtnBG","GroupSpotter_BtnBGOff",0,0)
	DynamicImageSetTexture("GroupSpotterSettingsWindowBtnBG","GroupSpotter_BtnBGOff",0,0)
end

function GroupSpotter.OnRButtonUp()
	GroupSpotter.ChangeMode( nil )
end

function GroupSpotter.toggle(set)
	if(type(set)~="boolean")
	then
		if (set==nil) then
			set=not GroupSpotterSettings.active
		else
			return
		end
	end
	if(set)
	then
		for _,k in ipairs(GroupSpotter.Maps) do
			for _,l in pairs(SystemData.MapPips) do
				if(pins[l]) then
					MapSetPinFilter(k.MapName,l,true)
				else
					MapSetPinFilter(k.MapName,l,false)
				end
			end
		end
		DynamicImageSetTexture("GroupSpotterBtn","GroupSpotter_BtnOn",0,0)
		DynamicImageSetTexture("GroupSpotterSettingsWindowBtn","GroupSpotter_BtnOn",0,0)
	else
		for _,k in ipairs(GroupSpotter.Maps) do
			for _,l in pairs(SystemData.MapPips) do
				if(k.MapPins[l] == nil) then
					k.MapPins[l] = true
				end
				MapSetPinFilter(k.MapName,l,k.MapPins[l])
			end
		end
		DynamicImageSetTexture("GroupSpotterBtn","GroupSpotter_BtnOff",0,0)
		DynamicImageSetTexture("GroupSpotterSettingsWindowBtn","GroupSpotter_BtnOff",0,0)
	end
	GroupSpotterSettings.active = set

	ButtonSetPressedFlag( "GroupSpotterSettingsWindow_SpotterCheckBoxButton", GroupSpotterSettings.active )
	if (GroupSpotterSettings.HealCircles.linkwithgroupspotter == true) then
		for _, hc in ipairs(GroupSpotter.HealCircles.registeredHealCircles) do
			hc:UpdateVisibility()
		end
	end
end

function GroupSpotter.VersionHandler()
	if ( not GroupSpotterSettings ) then
		GroupSpotterSettings = {}
		GroupSpotterSettings.active = false
		GroupSpotterSettings.version = 2.0
		GroupSpotterSettings.HealCircles = {}
		GroupSpotterSettings.HealCircles.linkwithgroupspotter = false
		GroupSpotterSettings.HealCircles.currentmode = 1
		GroupSpotterSettings.HealCircles.modesettings =
		{
			{
				single	= { alpha = 0.2, r = 0, g = 180, b = 180 },
				group	= { alpha = 0.2, r = 0, g = 0, b = 255 },
			},
			{
				single	= { alpha = 0.2, r = 0, g = 0, b = 100 },
				group	= { alpha = 0.2, r = 150, g = 150, b = 255 },
			}
		}

		GroupSpotter.firstrun = true
	else
		if(type(GroupSpotterSettings.version)=="string")
		then
			GroupSpotterSettings.version = 2.0
			local oldHC=GroupSpotterSettings.HC
			GroupSpotterSettings.HealCircles={}
			GroupSpotterSettings.HealCircles.linkwithgroupspotter=oldHC.linkwithgroupspotter
			GroupSpotterSettings.HealCircles.currentmode=oldHC.mode
			GroupSpotterSettings.HealCircles.modesettings =
			{
				{
					single	= { alpha = oldHC.alpha.single, r = oldHC.colors.single.r, g = oldHC.colors.single.g, b = oldHC.colors.single.b },
					group	= { alpha = oldHC.alpha.single2, r = oldHC.colors.group.r, g = oldHC.colors.group.g, b = oldHC.colors.group.b },
				},
				{
					single	= { alpha = oldHC.alpha.single, r = oldHC.colors.single.r2, g = oldHC.colors.single.g2, b = oldHC.colors.single.b2 },
					group	= { alpha = oldHC.alpha.group2, r = oldHC.colors.group.r2, g = oldHC.colors.group.g2, b = oldHC.colors.group.b2 },
				}
			}
			GroupSpotterSettings.HC=nil
		end
	end
end
